from collections import OrderedDict

import specter.navigators as sn
from specter import transform, setval, select


def test_increment_every_even_number():
    data = {'a': [{'aa': 1, 'bb': 2},
                  {'cc': 3}],
            'b': [{'dd': 4}]}
    transformed = transform([sn.map_vals, sn.each, sn.map_vals, sn.is_even],
                            lambda x: x + 1,
                            data)
    assert transformed == {'a': [{'aa': 1, 'bb': 3},
                                 {'cc': 3}],
                           'b': [{'dd': 5}]}


def test_append_a_seq_to_nested_vector():
    data = {'a': [1, 2, 3]}
    transformed = setval([sn.get('a'), sn.end],
                         [4, 5],
                         data)
    assert transformed == {'a': [1, 2, 3, 4, 5]}


def test_increment_last_odd_number():
    data = [1, 2, 3, 4, 5, 6, 7, 8]
    transformed = transform([sn.filterer(sn.is_odd), sn.last],
                            lambda x: x + 1,
                            data)
    assert transformed == [1, 2, 3, 4, 5, 6, 8, 8]


def test_map_func_over_seq_without_changing_type_or_order():
    for data, expected in [([1, 2, 3], [2, 3, 4]),
                           ({1, 2, 3}, {2, 3, 4}),
                           ((1, 2, 3), (2, 3, 4))]:
        transformed = transform(sn.each,
                                lambda x: x + 1,
                                data)
        assert data == expected


def test_retrieve_every_number_divisible_by_three_in_seq_of_seq():
    data = [[1, 2, 3, 4], [], [5, 3, 2, 18], [2, 4, 6], [12]]
    selected = select([sn.each, sn.each, lambda x: x % 3 == 0],
                      data)
    assert selected == [3, 3, 18, 6, 12]


def test_remove_none_from_a_nested_seq():
    data = {'a': [1, 2, None, 3, None]}
    transformed = setval([sn.get('a'), sn.each, sn.is_none],
                         sn.none,
                         data)
    assert transformed == {'a': [1, 2, 3]}


def test_remove_key_value_pair_from_a_dict():
    data = {'a': {'b': {'c': 1}}}
    transformed = setval([sn.get('a'), sn.get('b'), sn.get('c')],
                         sn.none,
                         data)
    assert transformed == {'a': {'b': {}}}

    transformed = setval([sn.get('a'), sn.compact(sn.get('b'), sn.get('c'))],
                         sn.none,
                         data)
    assert transformed == {}


def test_increment_all_the_odd_numbers_between_ranges_1_to_4():
    data = [0, 1, 2, 3, 4, 5, 6, 7]
    transformed = sn.transform([sn.srange(1, 4), sn.each, sn.is_odd],
                               lambda x: x + 1,
                               data)
    assert transformed == [0, 2, 2, 4, 4, 5, 6, 7]


def test_replace_the_subseq_of_indices_2_to_4():
    data = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
    transformed = sn.setval(sn.srange(2, 4),
                            ['a', 'b', 'c', 'd', 'e'],
                            data)
    assert transformed == [0, 1, 'a', 'b', 'c', 'd', 'e', 4, 5, 6, 7, 8, 9]


def test_concatenate_seq_to_ever_nested_seq():
    data = [[1], (1, 2), ['c']]
    transformed = sn.setval([sn.each, sn.end],
                            ['a', 'b'],
                            data)
    assert transformed == [[1, 'a', 'b'], (1, 2, 'a', 'b'), ['c', 'a', 'b']]


def test_get_all_numbers_no_matter_how_they_are_nested():
    data = OrderedDict([
        (2, [1, 2, [6, 7]]),
        ('a', 4),
        ('c', {'a': 1, 'd': [2, None]})
    ])
    selected = sn.select(sn.walker(sn.is_number),
                         data)
    assert selected == [2, 1, 2, 1, 2, 6, 7, 4]


def test_navigate_string_key():
    data = {'a': {'b': 10}}
    selected = sn.select([sn.get('a'), sn.get('b')],
                         data)
    assert selected == [10]


def test_reverse_positions_of_all_event_numbers_between_indices_4_to_11():
    data = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15]
    transformed = sn.transform([sn.srange(4, 11), sn.filterer(sn.is_even)],
                               reversed,
                               data)
    assert transformed == [0, 1, 2, 3, 10,
                           5, 8, 7, 6, 9, 4, 11, 12, 13, 14, 15]


def test_append_c_and_d_to_every_subseq_that_has_at_least_two_even_nums():
    data = [[1, 2, 3, 4, 5, 6], [7, 0, -1], [8, 8], []]
    transformed = sn.setval([sn.each,
                             sn.is_selected(
                                 sn.filterer(sn.is_even),
                                 sn.view(sn.count),
                                 sn.pred(lambda x: x >= 2)
                             ),
                             sn.end],
                            ['c', 'd'],
                            data)
    assert transformed == [[1, 2, 3, 4, 5, 6, 'c', 'd'],
                           [7, 0, -1], [8, 8, 'c', 'd'], []]


def test_collecting_one_level_up():
    data = [{'a': 1, 'b': 3}, {'a': 2, 'b': -10}, {'a': 4, 'b': 10}, {'a': 3}]
    transformed = sn.transform([sn.each,
                                sn.collect_one('b'),
                                sn.get('a'),
                                sn.is_even],
                               sum,
                               data)
    assert transformed == [{'b': 3, 'a': 1}, {'b': -10, 'a': -8},
                           {'b': 10, 'a': 14}, {'a': 3}]


def test_increment_value_by_10():
    data = {'a': 1, 'b': 3}
    transformed = transform([sn.get('a'), sn.putval(10)],
                            sum,
                            data)
    assert transformed == {'a': 11, 'b': 3}


def test_increment_if_complex_condition():
    data = [{'a': 2, 'c': [1, 2], 'd': 4}, {'a': 4, 'c': [0, 10, -1]},
            {'a': -1, 'c': [1, 1, 1], 'd': 1}]
    transformed = transform([sn.each(),
                             sn.if_path([sn.get('a'), sn.is_even],
                                        [sn.get('c'), sn.each],
                                        sn.get('d'))],
                            lambda x: x + 1,
                            data)
    assert transformed == [{'c': [2, 3], 'd': 4, 'a': 2},
                           {'c': [1, 11, 0], 'a': 4},
                           {'c': [1, 1, 1], 'd': 2, 'a': -1}]
